<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ImageRepository")
 */
class Image
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $originalName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $fileName;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\ImageOrder", mappedBy="image", cascade={"persist", "remove"})
     */
    private $imageOrder;

    public function __construct()
    {
        $this->date = new \DateTime('now');
    }

    public function __toString()
    {
        return $this->fileName;
    }

    public function __clone() {
        $this->id = null;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOriginalName(): ?string
    {
        return $this->originalName;
    }

    public function setOriginalName(string $originalName): self
    {
        $this->originalName = $originalName;

        return $this;
    }

    public function getFileName(): ?string
    {
        return $this->fileName;
    }

    public function setFileName(string $fileName): self
    {
        $this->fileName = $fileName;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getImageOrder(): ?ImageOrder
    {
        return $this->imageOrder;
    }

    public function setImageOrder(ImageOrder $imageOrder): self
    {
        $this->imageOrder = $imageOrder;

        // set the owning side of the relation if necessary
        if ($this !== $imageOrder->getImage()) {
            $imageOrder->setImage($this);
        }

        return $this;
    }
}
