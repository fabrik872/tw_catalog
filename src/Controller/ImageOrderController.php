<?php


namespace App\Controller;


use App\Entity\ImageOrder;
use App\Services\ImageSaver;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ImageOrderController extends AbstractController
{

    /**
     * @Route("/image_order/sort", name="sort_image_order")
     */
    public function sort(Request $request)
    {
        $items = $request->get('item');
        $em = $this->getDoctrine()->getManager();

        foreach ($items as $key => $itemId) {
            $imageOrder = $em->getRepository(ImageOrder::class)->find($itemId);

            $imageOrder->setXOrder($key);
            $em->persist($imageOrder);
        }
        $em->flush();

        return $this->json($items);
    }

    /**
     * @Route("/image_order/remove", name="remove_image_order", methods={"POST"})
     */
    public function remove(Request $request, ImageSaver $imageSaver)
    {
        if ($this->isCsrfTokenValid('removeImageOrder', $request->get('csfr_token'))) {
            $imageOrders = $request->get('image');
            $em = $this->getDoctrine()->getManager();

            foreach ($imageOrders as $imageOrder) {
                $imageOrder = $em->getRepository(ImageOrder::class)->find($imageOrder);
                $imageSaver->loadImage($imageOrder->getImage());
                $imageSaver->delete();
                $em->remove($imageOrder);
            }
            $em->flush();
            $referer = $request->headers->get('referer');
            return new RedirectResponse($referer);
        }
        throw new \Exception('Invalid csfr token');
    }
}