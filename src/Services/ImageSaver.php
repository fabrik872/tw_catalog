<?php


namespace App\Services;


use App\Entity\Image;
use App\Entity\Images;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Asset\Package;
use Symfony\Component\Asset\VersionStrategy\EmptyVersionStrategy;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;

class ImageSaver
{
    private $param;
    /** @var \App\Entity\Image $image */
    private $image;
    private $flash;
    private $em;
    private $uploadFolder;

    public function __construct(ParameterBagInterface $parameterBag, FlashBagInterface $flashBag, EntityManagerInterface $em)
    {
        $this->param = $parameterBag;
        $this->flash = $flashBag;
        $this->em = $em;
        $this->uploadFolder = $this->param->get('kernel.project_dir') . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . $this->param->get('uploadFolder');
    }

    public function saveFile(UploadedFile $uploadedFile): void
    {
        $fileName = md5(base64_encode(fread(fopen($uploadedFile->getRealPath(), 'r'), filesize($uploadedFile->getRealPath()))));

        $this->image = new Image();
        $this->image->setFileName($fileName . '.' . $uploadedFile->guessExtension());
        $this->image->setOriginalName($uploadedFile->getClientOriginalName());
        $this->imageId = $this->image;
        $uploadedFile->move($this->uploadFolder, $fileName . '.' . $uploadedFile->guessExtension());
    }

    public function loadImage(Image $images): void
    {
        $this->image = $images;
    }

    public function delete(): bool
    {
        if ($this->em->getRepository(Image::class)->getImageUses($this->image) <= 1) {
            try {
                $imageData = new \Gregwar\Image\Image($this->imageLink($this->image));
                $this->flash->add('success', '<img class="img-thumbnail" src="data:image/jpeg;base64, ' . base64_encode($imageData->resize(null, 50)->get()) . '">  was removed');
                unlink($this->uploadFolder . DIRECTORY_SEPARATOR . $this->image->getFileName());
            } catch (\Exception $exception) {
                $this->flash->add('danger', 'Error removing image ' . $this->image->getOriginalName() . ' \nError: ' . $exception->getMessage());
                return false;
            }
        }

        $this->em->remove($this->image);
        $this->em->flush();

        return true;
    }

    public function getImage(): ?Image
    {
        return $this->image;
    }

    public function imageLink(?string $image)
    {
        if ($image == null) {
            $package = new Package(new EmptyVersionStrategy());

            return $package->getUrl('assets/img/default.png');
        }
        return $this->param->get("uploadFolder") . DIRECTORY_SEPARATOR . $image;
    }
}
