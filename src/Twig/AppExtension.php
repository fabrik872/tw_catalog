<?php

namespace App\Twig;

use App\Entity\Image;
use App\Services\ImageSaver;
use Doctrine\ORM\EntityManagerInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

class AppExtension extends AbstractExtension
{
    private $em;
    private $imageSaver;

    public function __construct(EntityManagerInterface $em, ImageSaver $imageSaver)
    {
        $this->em = $em;
        $this->imageSaver = $imageSaver;
    }

    public function getFilters(): array
    {
        return [
            // If your filter generates SAFE HTML, you should add a third
            // parameter: ['is_safe' => ['html']]
            // Reference: https://twig.symfony.com/doc/2.x/advanced.html#automatic-escaping
            new TwigFilter('bytes', [$this, 'readableBytes']),
            new TwigFilter('imageLink', [$this, 'imageLink']),
        ];
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('uniqueId', [$this, 'getUniqueId'])
        ];
    }


    public function readableBytes($bytes)
    {
        if ($bytes) {
            $i = floor(log($bytes) / log(1024));
        } else {
            return "0 B";
        }
        $sizes = array('B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');

        return sprintf('%.02F', $bytes / pow(1024, $i)) * 1 . ' ' . $sizes[$i];
    }

    public function imageLink(?Image $image)
    {
        return $this->imageSaver->imageLink($image);
    }

    /**
     * getUniqueId
     *
     * @return string
     */
    public function getUniqueId(?string $prefix = ''): string
    {
        return uniqid($prefix);
    }
}
