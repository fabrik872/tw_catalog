<?php

namespace App\Services;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Twig\Environment;

class Datatable
{
    private $em;

    /**@var $qb \Doctrine\ORM\QueryBuilder */
    private $qb;

    /**@var $renderer Environment */
    private $renderer;

    /**@var $request Request */
    private $request;
    private $uniqueId;
    private $columns = [];
    private $tableColumns = [];
    private $templateAddress;
    private $urlGenerator;

    public function __construct(
        EntityManagerInterface $em,
        Environment $renderer,
        UrlGeneratorInterface $urlGenerator
    )
    {
        $this->em = $em;
        $this->renderer = $renderer;
        $this->urlGenerator = $urlGenerator;
    }

    public function setQueryBuilder(QueryBuilder $qb, array $columns, array $tableColumns, Request $request)
    {
        $this->qb = $qb;
        $this->request = $request;
        $this->columns = $columns;
        $this->tableColumns = $tableColumns;
        $this->uniqueId = md5($qb->getQuery()->getDQL());
    }

    public function createTable(string $templateAddress = 'partials/_datatables.html.twig', array $templateParams = [])
    {
        $this->templateAddress = $templateAddress;
        $template = $this->renderer->loadTemplate($templateAddress);

        foreach ($this->tableColumns as $tableColumn) {
            $columnNames[] = substr($tableColumn, strpos($tableColumn, '.'));
        }

        return $template->renderBlock('datatable', array_merge([
            'columns' => self::getTableNames($this->tableColumns),
            'uniqueId' => $this->uniqueId,
        ], $templateParams));
    }

    public function createJs(string $route = '')
    {
        if ($route == '') {
            $route = $this->request->getRequestUri();
        }

        $template = $this->renderer->loadTemplate($this->templateAddress);

        return $template->renderBlock('javascripts', [
            'columns' => self::getTableNames($this->columns),
            'route' => $route,
            'uniqueId' => $this->uniqueId,
        ]);
    }

    public function ajax(Request $request, string $alias): JsonResponse
    {
        $this->request = $request;
        $return = new \stdClass();
        $return->draw = $request->get('draw');
        $return->recordsTotal = $this->getTotalRows($alias);
        $return->recordsFiltered = $this->getFilteredRows($alias, $request);
        $return->data = $this->getData($alias, $request);

        return new JsonResponse($return, 200);
    }

    private function getTotalRows(string $alias): int
    {
        $qb = $this->qb;
        $qb->select('count(' . $alias . ')');

        return $qb->getQuery()->getSingleScalarResult();
    }

    private function getFilteredRows(string $alias, Request $request): int
    {
        $this->request = $request;
        $qb = $this->qb;
        $qb->select('count(' . $alias . ')');
        foreach ($request->get('columns') as $key => $column) {
            $columnName = $this->columns[$key];
            $qb->andWhere($columnName . ' LIKE :x' . $key);
            $qb->setParameter('x' . $key, '%' . $column['search']['value'] . '%');
        }
        return $qb->getQuery()->getSingleScalarResult();
    }

    private function getData(string $alias, ?Request $request = null)
    {
        $qb = $this->qb;
        $qb->select($alias);
        $select = '';
        foreach ($this->columns as $key => $column) {
            $select .= $column;
            if (count($this->columns) != $key + 1) {
                $select .= ', ';
            }
        }
        $qb->select($select);
        foreach ($request->get('order') as $order) {
            if ($order['dir'] == 'desc') {
                $dir = 'DESC';
            } else {
                $dir = 'ASC';
            }
            $qb->orderBy($this->columns[$order['column']], $dir);
        }
        $qb->setFirstResult($request->get('start'));
        if ($request->get('length') > 0)
            $qb->setMaxResults($request->get('length'));

        return $qb->getQuery()->getArrayResult();
    }

    private static function getTableNames(array $tableColumns)
    {
        $columnNames = [];
        foreach ($tableColumns as $tableColumn) {
            $columnNames[] = substr($tableColumn, strpos($tableColumn, '.') + 1);
        }
        return $columnNames;
    }

    /**
     * @return bool
     */
    public function isDatatable(): bool
    {
        $draw = $this->request->get('draw');
        return isset($draw);
    }
}