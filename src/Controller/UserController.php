<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\ChangePasswordType;
use App\Model\ChangePasswordModel;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/user", name="user.")
 */
class UserController extends AbstractController
{
    /**
     * @Route("/", name="index", methods={"GET"})
     */
    public function index(UserRepository $userRepository): Response
    {
        return $this->render('user/index.html.twig', [
            'users' => $userRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="new", methods={"GET","POST"})
     *
     * NOTE: new user will be possible to add just via registration
     */
    public function new(Request $request): Response
    {
        return $this->redirectToRoute('app_register');
    }

    /**
     * @Route("/{id}", name="show", methods={"GET"})
     */
    public function show(User $user): Response
    {
//        TODO: show also favorites products of actual user

        $ahoj = $this->isGranted('view', $user);

        if (!$ahoj) {
            throw new NotFoundHttpException('Nemas opravnenie pozerat na inych uzivatelov');
        }

        return $this->render('user/show.html.twig', [
            'user' => $user,
        ]);
    }


    /**
     * @Route("/change-password/{id}", name="change_password")
     *
     * @param Request $request
     * @param User $user
     * @return Response
     */
    public function editUserPassword(Request $request, User $user): Response
    {
        $modelForm = new ChangePasswordModel();
        $form = $this->createForm(ChangePasswordType::class, $modelForm);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $newPassword = $modelForm->getNewPassword();
            $passwordHashed = password_hash($newPassword, PASSWORD_DEFAULT);

            $user->setPassword($passwordHashed);
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', 'Vase heslo bolo zmenene');

            return $this->redirectToRoute('user.change_password', ['id'=> $user->getId()]);
        }

        return $this->render('user/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="delete", methods={"DELETE"})
     */
    public function delete(Request $request, User $user): Response
    {
        if ($this->isCsrfTokenValid('delete'.$user->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($user);
            $entityManager->flush();
        }

        return $this->redirectToRoute('index');
    }
}
