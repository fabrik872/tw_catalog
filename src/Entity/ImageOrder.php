<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ImageOrderRepository")
 */
class ImageOrder
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $xOrder;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Image", inversedBy="imageOrder", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $image;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Product", inversedBy="photos")
     */
    private $product;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getXOrder(): ?int
    {
        return $this->xOrder;
    }

    public function setXOrder(int $xOrder): self
    {
        $this->xOrder = $xOrder;

        return $this;
    }

    public function getImage(): ?Image
    {
        return $this->image;
    }

    public function setImage(Image $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }
}
