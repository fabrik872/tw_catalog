<?php

namespace App\Repository;

use App\Entity\ImageOrder;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ImageOrder|null find($id, $lockMode = null, $lockVersion = null)
 * @method ImageOrder|null findOneBy(array $criteria, array $orderBy = null)
 * @method ImageOrder[]    findAll()
 * @method ImageOrder[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ImageOrderRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ImageOrder::class);
    }

    // /**
    //  * @return ImageOrder[] Returns an array of ImageOrder objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ImageOrder
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
