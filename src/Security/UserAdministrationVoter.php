<?php


namespace App\Security;


use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class UserAdministrationVoter extends Voter
{
    // these strings are just invented: you can use anything
    const VIEW = 'view';
    const DELETE = 'delete';

    protected function supports($attribute, $subject) : bool
    {
        // if the attribute isn't one we support, return false
        if (!in_array($attribute, [self::VIEW, self::DELETE])) {
            return false;
        }

        // only vote on Post objects inside this voter
        if (!$subject instanceof User) {
            return false;
        }

        return true;
    }

    protected function voteOnAttribute($attribute, $grantedUser, TokenInterface $token) : bool
    {
        $user = $token->getUser();

        if (!$user instanceof User) {
            // the user must be logged in; if not, deny access
            return false;
        }

        /** @var User $grantedUser */

        switch ($attribute) {
            case self::VIEW:
                return $this->canView($grantedUser, $user);
            case self::DELETE:
                return $this->canDelete($user->getRoles());
        }

        throw new \LogicException('This code should not be reached!');
    }

    private function canView(User $grantedUser, User  $user) : bool
    {
        return $grantedUser === $user;
    }

    private function canDelete(array $roles) : bool
    {
        return in_array(User::ADMIN, $roles);
    }
}
