<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Product;
use Doctrine\ORM\EntityManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/", name="default.")
 *
 * @Security("is_granted('ROLE_USER')")
 */
class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function index()
    {
        $products = $this->_em()->getRepository(Product::class)->findAll();
        $categories = $this->_em()->getRepository(Category::class)->findAll();

        return $this->render('default/index.html.twig', [
            'products' => $products,
            'categories' => $categories,
        ]);
    }

    /**
     * @Route("/show/{id}", name="show")
     */
    public function show(Product $product)
    {
        return $this->render('default/show.html.twig', [
            'product' => $product,
        ]);
    }

    /**
     * @Route("/{id}", name="byCategory")
     */
    public function byCategory(Category $category)
    {
        $categories = $this->_em()->getRepository(Category::class)->findAll();

        return $this->render('default/index.html.twig', [
            'products' => $category->getProducts(),
            'categories' => $categories,
        ]);
    }

    private function _em(): EntityManager
    {
        return $this->getDoctrine()->getManager();
    }
}
