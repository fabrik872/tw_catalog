<?php

namespace App\Controller;

use App\Entity\ImageOrder;
use App\Entity\Product;
use App\Form\ProductType;
use App\Repository\ProductRepository;
use App\Services\Datatable;
use App\Services\ImageSaver;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/product", name="product.")
 *
 * @Security("is_granted('ROLE_ADMIN')")
 */
class ProductController extends AbstractController
{
    /**
     * @Route("/", name="index", methods={"GET"})
     */
    public function index(ProductRepository $productRepository, Datatable $datatable, Request $request): Response
    {
        $qb = $this->getDoctrine()->getManager()->getRepository(Product::class)->createQueryBuilder('p');

        $datatable->setQueryBuilder(
            $qb,
            ['p.title', 'p.stock', 'p.price', 'p.createdAt', 'p.id'],
            ['p.title', 'p.stock', 'p.price', 'p.createdAt'],
            $request);
        if ($datatable->isDatatable()) {
            return $datatable->ajax($request, 'p');
        }

        return $this->render('product/index.html.twig', [
            'datatable' => $datatable->createTable('partials/_product_datatables.html.twig'),
            'datatableJs' => $datatable->createJs(),
            'products' => $productRepository->findAll(),

        ]);
    }

    /**
     * @Route("/new", name="new", methods={"GET","POST"})
     */
    public function new(Request $request, ImageSaver $imageSaver): Response
    {
        $product = new Product();
        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $uploadedimages = $form->get('photos')->getData();
            $this->saveImages($uploadedimages, $product, $imageSaver);
            $entityManager->persist($product);
            $entityManager->flush();

            return $this->redirectToRoute('product.index');
        }

        return $this->render('product/new.html.twig', [
            'product' => $product,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="show", methods={"GET"})
     *
     * @Security("is_granted('ROLE_USER')")
     */
    public function show(Product $product): Response
    {
        return $this->render('product/show.html.twig', [
            'product' => $product,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Product $product, ImageSaver $imageSaver): Response
    {
        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $uploadedimages = $form->get('photos')->getData();
            $this->saveImages($uploadedimages, $product, $imageSaver);

            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('product.edit', ['id' => $product->getId()]);
        }

        return $this->render('product/edit.html.twig', [
            'product' => $product,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="delete", methods={"DELETE"})
     */
    public function delete(Request $request, Product $product, ImageSaver $imageSaver): Response
    {
        if ($this->isCsrfTokenValid('delete' . $product->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            foreach ($product->getPhotos() as $imageOrder) {
                $imageSaver->loadImage($imageOrder->getImage());
                $imageSaver->delete();
                $entityManager->remove($imageOrder);
            }
            $this->addFlash('success', 'Produkt ' . $product->getTitle() . ' bol uspense zmazany');
            $entityManager->remove($product);
            $entityManager->flush();
        }

        return $this->redirectToRoute('product.index');
    }

    private function saveImages(array $images, Product $product, ImageSaver $imageSaver)
    {
        foreach ($images as $uploadedFile) {
            $imageOrder = new ImageOrder();
            $imageSaver->saveFile($uploadedFile);
            $imageOrder->setImage($imageSaver->getImage());
            $imageOrder->setProduct($product);
            $this->getDoctrine()->getManager()->persist($imageOrder);
            $this->addFlash('success', 'Photo ' . $imageSaver->getImage()->getOriginalName() . ' added');
        }
    }
}
